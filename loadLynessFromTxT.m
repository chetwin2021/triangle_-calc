function TriCoeff = loadLynessFromTxT(filename)
TriCoeff=cell(22,1);
fp=fopen(filename,'r');
data=textscan(fp,'%f,%f,%f');
fclose(fp);
ALL=[data{1,1},data{1,2},data{1,3}];
[N,~]=size(ALL);
i=1;
while i<N
   rule=ALL(i,1);
   order=ALL(i,2);
   TriCoeff{rule+1,1}=ALL(i+1:i+order,:);
   i=i+order+1;
end
end

