function [P_W] = getTrianglePoints(Triangle,Rule)
% getTrianglePoints 三角形面元积分
% https://people.sc.fsu.edu/~jburkardt/cpp_src/triangle_lyness_rule/triangle_lyness_rule.html
%   输入：
%   Triangle(3,3)：三角形面元三个点
%   Rule：triangle_lyness_rule
%   输出：
%   P_W(:,4)：P_W(:,1:3)积分点、P_W(:,4)权重系数

%% 任意空间三角形 =》平面直角三角形 坐标转换
% 形函数
N1=@(s,t) -s-t+1;
N2=@(s,t) s;
N3=@(s,t) t;
N1_s=@(s,t) -1;
N2_s=@(s,t) 1;
N3_s=@(s,t) 0;
N1_t=@(s,t) -1;
N2_t=@(s,t) 0;
N3_t=@(s,t) 1;

P1=Triangle(1,:);
P2=Triangle(2,:);
P3=Triangle(3,:);

global TriCoeff;
data=TriCoeff{Rule+1,1};
[order,~]=size(data);
P_W=zeros(order,4);

for i=1:1:order
    P_W(i,1:3)=Loc2Glo(data(i,1:2));
    P_W(i,4)=data(i,3)*Jacobi(data(i,1:2));
end
    function Pglobal=Loc2Glo(loc)
        %loc(1,2)
        Pglobal=N1(loc(1),loc(2))*P1+...
            N2(loc(1),loc(2))*P2+...
            N3(loc(1),loc(2))*P3;
    end

    function J=Jacobi(Loc)
        s=N1_s(Loc(1),Loc(2))*P1+...
            N2_s(Loc(1),Loc(2))*P2+...
            N3_s(Loc(1),Loc(2))*P3;
        t=N1_t(Loc(1),Loc(2))*P1+...
            N2_t(Loc(1),Loc(2))*P2+...
            N3_t(Loc(1),Loc(2))*P3;
        %三角形，这里多除了一个2
        J=norm(cross(s,t))/2;
    end
end

