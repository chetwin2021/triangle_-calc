%% 测试三角形积分
clc;clear;
global TriCoeff
TriCoeff=loadLynessFromTxT('LYNESS_RULE.txt');
P1=[0,0,0];
P2=[2,0,0];
P3=[0,3,0];
% 积分
func=@(x,y,z) log(x^2+y^2+1);

count=1;
for rule=0:1:21
    [P_W] = getTrianglePoints([P1;P2;P3],rule);
    [N,~]=size(P_W);
    res=0;
    for i=1:1:N
        res=res+func(P_W(i,1),P_W(i,2),P_W(i,3))*P_W(i,4);
    end
    resA(count,1)=res;
    resA(count,2)=rule;
    resA(count,3)=N;
    count=count+1;
end

%%
pfun = @(x,y) log(x.^2+y.^2+1);
xmin = 0;
xmax = 2;
ymin = 0;
ymax = @(x) -3/2*x+3;
r = integral2(pfun,xmin,xmax,ymin,ymax);
%% plot
figure(22)
plot(resA(:,2),resA(:,1),'r-o');hold on;
plot(resA(:,2),r*ones(22,1),'b-');grid on;
xticks([0:2:22]);
xlim([0,22]);
legend('TRIANGLE LYNESS RULE 积分','Matlab integral2积分');
text(10,3,'积分函数：log(x^2+y^2+1)')
text(10,2.9,'积分区域：(0,0,0),(2,0,0),(0,3,0)');
xlabel('Lyness Rule');
ylabel('积分数值');