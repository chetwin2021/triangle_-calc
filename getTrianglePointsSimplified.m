function [P_W] = getTrianglePointsSimplified(Triangle,Rule)
    global TriCoeff;
    points = TriCoeff{Rule+1};
    weights = points(:,3)';
    points(:,3) = 1-points(:,1)-points(:,2);
    P_W = zeros(size(points,1),4);
    P_W(:,1:3)=points*Triangle;
    area = 0.5*norm(cross(Triangle(1,:)-Triangle(2,:),Triangle(1,:)-Triangle(3,:)));
    P_W(:,4)=weights*area;
end

